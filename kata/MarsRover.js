class MarsRover {
    constructor(location, direction) {
        this.location = location;
        this.direction = direction || "N";
        
    }

    commands(keys){
    //Takes an array of commands 
    var location = this.location;
    var direction = this.direction;

        keys.forEach(function(key){
            if (key == "f" && direction == "N"){
                location[1] += 1;
            } else if (key == "f" && direction == "S"){
                location[1] -= 1;
            } else if (key == "f" && direction == "W"){
                location[0] -= 1;
            } else if (key == "f" && direction == "E"){
                location[0] += 1;
            } else if (key == "b" && direction == "S"){
                location[1] += 1;
            } else if (key == "b" && direction == "W"){
                location[0] += 1;
            } else if (key == "b" && direction == "E"){
                location[0] -= 1;
            } else if (key == "b" && direction == "N"){
                location[1] -= 1;
            } else if (key == "l" && direction == "E"){
                direction = "N";
            } else if (key == "l" && direction == "N"){
                direction = "W";
            } else if (key == "r" && direction == "E"){
                direction = "S";
            } 
        })
        this.location = location;
        this.direction = direction;
    };


}

module.exports = MarsRover;

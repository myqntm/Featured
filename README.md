# Portfolio


## About

Inside you will find a selection of my projects.

### Udacity Data Analyst Nanodegree
This contains projects from the nanodegree that I'm currently in.




### Pangaea's Box Book Recommender

After scraping information on literature from various websites, I decided to share my dataset with the world by creating a random book generator. This is very much version 1 so there is much to come. Live version can be found [here](https://pangaeasbox.github.io/).

### Front End Projects
##### Local Weather App
This was part of the FreeCodeCamp front end project requirements. You can see a live demo [here on CodePen](https://codepen.io/krichoo/pen/aGzzXp). I decided to start simple and add on as needed.

##### Random Quote Engine
This was also a part of the FreeCodeCamp front end project requirements. You can see a live demo [here on CodePen](https://codepen.io/krichoo/pen/OveVzj). 

##### Wikipedia Viewer
This was also a part of the FreeCodeCamp front end project requirements. You can see a live demo [here on CodePen](https://codepen.io/krichoo/pen/wjKONG). 

##### Pixel Art Maker
This was a part of the Google/Udacity Web Developer Challenge Scholarship. You can see a live demo [here on CodePen](https://codepen.io/krichoo/pen/bLyReo). 


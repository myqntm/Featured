# Project 1

## Exploring Weather Trends

By Cathy Richards

### Extract the Data

_Tools Used: SQL_

Before running any queries, I first examined each of the 3 datasets in the schema. From there I was able to get a general idea of what columns I was to expect.

In order to select the city I ran the following query:

``` sql
SELECT * FROM city_list
```

Since my city (San Jose, Costa Rica) was not available, I selected Cali, Colombia as neither Panama City nor Medellin were available.

Next, I selected the weather data for Cali from the city_data database using the following query:

``` sql
SELECT * FROM city_data

WHERE city LIKE 'Cali'
```

Finally, I ran the following query to obtain the global weather values:

``` sql
SELECT * FROM global_data
```

### Analysis

_Tools Used: R_

Now that I have the datasets I need, I chose to use R as it's the software I am currently most comfortable with.

#### Preparing the Dataset

First, I imported the global weather and Cali weather datasets:

``` r
globalweather <- read.csv("~/DAND/global.csv")

localweather <- read.csv("~/DAND/cali.csv")
```

After examining that both datasets were imported correctly, I merged them in order to be able to graph them together. In hindsight, I realize now that I could have merged the 2 datasets in SQL and removed unnecessary columns such as the city and country.

``` r
newdf <- merge(localweather, globalweather, by.x = "year", by.y = "year")
```


Additionally, I renamed  the columns so that they would be easier to understand.

``` r
names(newdf) <- c("Year", "City", "Country", "Cali_Weather", "Global_Weather")
```

Next I used the 'RcppRoll' package to calculate the rolling mean.

``` r
# Import library

library('RcppRoll')

# Add moving average column to dataset.

newdf$moving_average <- roll_mean(newdf$Cali_Weather, 15, align="right", fill=0)
```

 By telling the function to align right, I'm directing to use the previous 15 observations to calculate the mean. The fill tells the function to fill in the first 14 observations with 0.

In order to plot the temperatures, I subsetted the dataset so that it only included values above 0. This would effectively remove all the fills as well as missing observations.

``` r
newdf <- subset(newdf, moving_average > 0)
```

### Plotting

I chose ggplot2 to create the line charts as I prefer the way piece meal it creates charts. Additionally, I wanted to display each line clearly as a different color and felt that ggplot was better at that over baseR.

``` r
# Import ggplot2

library("ggplot2")

# Create chart

ggplot(newdf, aes(Year)) +

  geom_line(aes(y = Global_Weather, color = "Global Weather")) +

  geom_line(aes(y = Moving_Average, color = "Cali, Colombia")) +

  labs(title="Global Weather Trends", x = "Year", y = "Weather", colour = "Location")
```

#### Output

![alt text](Portfolio/Data Analyst Nanodegree/Global Weather/Picture1.png) 


Observations:

1. Multiple reports have been released from scientists around the world which inform us that the global temperature is indeed warming. Therefore, it is not a surprise to see the same trend in this data. The average global weather has increased by nearly 2 degrees since the 1800s.

2. Due to the fact that my city is located in the tropics, the average weather in Cali, Colombia is significantly higher than the global average temperature. On average, the temperature in Cali is 13.24 degrees warmer.

3. Shortly after 1875, there was a sharp increase in the global weather temperature. A similar increase can be seen in the temperature in Colombia during that time though it's not as severe.

4. Both the global and local Cali temperatures were relatively stable until 1925 when both began to trend upward.
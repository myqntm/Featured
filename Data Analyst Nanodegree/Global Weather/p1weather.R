
install.packages("RcppRoll")
library('RcppRoll')
library('dplyr')
library("ggplot2")

# Import datasets
globalweather <- read.csv("~/mente/Tier2/DAND/global.csv")
localweather <- read.csv("~/mente/Tier2/DAND/cali.csv")


newdf <- merge(localweather, globalweather, by.x = "year", by.y = "year")
names(newdf) <- c("Year", "City", "Country", "Cali_Weather", "Global_Weather")

newdf$Moving_Average <- roll_mean(newdf$Cali_Weather, 15, align="right", fill=0)

newdf <- subset(newdf, Moving_Average > 0)


ggplot(newdf, aes(Year)) + 
  geom_line(aes(y = Global_Weather, color = "Global Weather")) + 
  geom_line(aes(y = Moving_Average, color = "Cali, Colombia")) +
  labs(title="Global Weather Trends", x = "Year", y = "Weather", colour = "Location")

  




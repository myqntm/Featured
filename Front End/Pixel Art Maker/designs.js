function makeGrid() {
    var canvas, canvasCell, height, width, rows;
  
    canvas = $('#pixelCanvas');  
    height = $('#inputHeight').val();
    width = $('#inputWeight').val();
  
  	canvas.children().remove()

    for (i = 1; i <= height; i++){
        //Append row to table
        canvas.append('<tr></tr>');
    }
  
    rows = $('tr');

    for (j = 1; j <= width; j++){
        //Append column to table
        rows.append('<td></td>')
      };
  
  canvasCell = canvas.find('td');

  
  canvasCell.click(function(){
    var color = $('#colorPicker').val();
    $(this).attr("bgcolor", color);
  
}) 
  
 };

// When size is submitted by the user, call makeGrid()
var submitQuery = $('input[type="submit"]')

submitQuery.click(function(event) {
  event.preventDefault();
  makeGrid();
});



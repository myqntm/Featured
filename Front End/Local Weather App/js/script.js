var options = {
  enableHighAccuracy: false, 
  timeout: 5000,  
  maximumAge: 0 
};

function error(err){
  alert('Error: ' + err + ' :('); // alert the error message
}

if('geolocation' in navigator){
  navigator.geolocation.getCurrentPosition(success);
}
function success(pos){
 var lat = pos.coords.latitude;
 var long = pos.coords.longitude;

 var mainUrl = "https://fcc-weather-api.glitch.me/api/current?lat=";

 var apiRequest = mainUrl + lat + "&lon=" + long; 

 fetch(apiRequest)
 .then((resp) => resp.json()) // Transform the data into json
 .then(function(data) {
  var desc = data.weather[0].main;
  var temp = data.main.temp;
  temp = temp.toFixed(0);
  var iconURL = data.weather[0].icon; 
  $(".temperature").text(temp);
  $(".measurement").text("C");
  $(".description").text(desc);
  $("img").attr("src", iconURL);
  });
}

var unitC = true;


function Convert(pos){
  if (unitC){
    var temp = Number($(".temperature")["0"].innerHTML);
    var tempNew = temp * (9/5) + (32);
    tempNew = tempNew.toFixed(0);
    $(".temperature").text(tempNew);
    $(".measurement").text("F");
    unitC = false;
  } else {
    var temp = Number($(".temperature")["0"].innerHTML);
    var tempNew = (temp - 32)/1.8;
    tempNew = tempNew.toFixed(0);
    $(".temperature").text(tempNew);
    $(".measurement").text("C");
    unitC = true;
  }

  
}